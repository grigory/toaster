\name{getNumericTypes}
\alias{getNumericTypes}
\title{List Aster numeric data types.}
\usage{
  getNumericTypes()
}
\value{
  character vector with names of Aster numeric data types
}
\description{
  List Aster numeric data types.
}
\examples{
getNumericTypes()
}

